/**
 * 
 */
package com.geeklabs.cloudbasedstudentdata;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

/**
 * @author Mahesh Yadav
 * 
 */
public class SelectDetails extends Activity {

	private Spinner spinner2, spinner3;
	private Button btnSubmit;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_details);
		
		addItemsOnBranchSpinner();
		addItemsOnYearSpinner();
		
		

		findViewById(R.id.go_button).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(getApplicationContext(),
						StudentDetails.class);
				startActivity(intent);
			}
		});
	}

	private void addItemsOnBranchSpinner() {

		spinner2 = (Spinner) findViewById(R.id.branch_spinner);
		List<String> list = new ArrayList<String>();
		list.add("CSE");
		list.add("IT");
		list.add("ECE");
		list.add("EEE");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(dataAdapter);
	}

	private void addItemsOnYearSpinner() {

		spinner3 = (Spinner) findViewById(R.id.year_spinner);
		List<String> list = new ArrayList<String>();
		list.add("2010");
		list.add("2011");
		list.add("2012");
		list.add("2013");
		list.add("2014");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner3.setAdapter(dataAdapter);
	}

}
