package com.geeklabs.cloudbasedstudentdata;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SearchView;
import android.widget.SearchView.OnSuggestionListener;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class StudentDetails extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.student_details);

		TextView maheshID = (TextView) findViewById(R.id.nametextView1);
		TextView baluID = (TextView) findViewById(R.id.nametextView2);
		TextView shyluID = (TextView) findViewById(R.id.nametextView3);
		TextView rajuID = (TextView) findViewById(R.id.nametextView4);

		final String mahesh = maheshID.getText().toString();
		final String balu = baluID.getText().toString();
		final String shylu = shyluID.getText().toString();
		final String raju = rajuID.getText().toString();

		findViewById(R.id.gettextView1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
				intent.putExtra("mahesh_name", mahesh);
				startActivity(intent);
			}
		});
		findViewById(R.id.gettextView2).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
				intent.putExtra("balu_name", balu);
				startActivity(intent);
			}
		});

		findViewById(R.id.gettextView3).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
				intent.putExtra("shylu_name", shylu);
				startActivity(intent);
			}
		});

		findViewById(R.id.gettextView4).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
				intent.putExtra("raju_name", raju);
				startActivity(intent);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		int[] to = new int[] { R.id.studentnamelist_textView1, R.id.studentId_textView2 };
		String[] from = new String[] { "student_name", "student_id" };

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		// To show search widget
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

		searchView.setQueryHint("Search student");
		searchView.setOnSuggestionListener(new OnSuggestionListener() {

			@Override
			public boolean onSuggestionSelect(int position) {
				return false;
			}

			@Override
			public boolean onSuggestionClick(int position) {
				return true;
			}

		});
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(true);

		Cursor cursor = null;
		
		// TODO need to get details from server
		/*
		 * if
		 * (SQLLiteManager.getInstance(getApplicationContext()).isTableExists(
		 * SQLLite.MARK_INFO_TABLE_NAME)) { cursor =
		 * SQLLiteManager.getInstance(getApplicationContext
		 * ()).getLocationNameList(); }
		 */
		
		

		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.student_list, cursor, from, to, 0);

		searchView.setSuggestionsAdapter(adapter);
		return super.onCreateOptionsMenu(menu);

	}
}
