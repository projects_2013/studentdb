/**
 * 
 */
package com.geeklabs.cloudbasedstudentdata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class DetailsActivity extends Activity {

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.details);
		
		TextView studentName = (TextView) findViewById(R.id.student_name);
		
		
		Intent intent = getIntent();
		String mahi = intent.getStringExtra("mahesh_name");
		String balu = intent.getStringExtra("balu_name");
		String shylu = intent.getStringExtra("shylu_name");
		String raju = intent.getStringExtra("raju_name");
		
		if (mahi != null && mahi.equalsIgnoreCase("mahesh") && !mahi.isEmpty()) {
			studentName.setText(mahi);
		}else if (balu != null && balu.equalsIgnoreCase("balu") && !balu.isEmpty()) {
			studentName.setText(balu);
		}
		else if (shylu != null && shylu.equalsIgnoreCase("shylu") && !shylu.isEmpty()) {
			studentName.setText(shylu);
		}
		else if (raju != null && raju.equalsIgnoreCase("raj") && !raju.isEmpty()) {
			studentName.setText(raju);
		}
		
		
		findViewById(R.id.exam_Button).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),ExamDetailsActivity.class);
				startActivity(intent);
			}
		});
		findViewById(R.id.button3).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),MarksActivity.class);
				startActivity(intent);
			}
		});
		findViewById(R.id.button4).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),AttendanceActivity.class);
				startActivity(intent);
			}
		});
	}

}
