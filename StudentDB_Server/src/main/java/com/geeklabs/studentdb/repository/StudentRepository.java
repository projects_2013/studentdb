package com.geeklabs.studentdb.repository;

import java.util.List;

import com.geeklabs.studentdb.domain.Student;
import com.geeklabs.studentdb.repository.objectify.ObjectifyCRUDRepository;

public interface StudentRepository extends ObjectifyCRUDRepository<Student>  {
	
	List<Student> getStudentsInfo(long studentId);
}
