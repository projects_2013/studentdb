package com.geeklabs.studentdb.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import com.geeklabs.studentdb.domain.User;
import com.geeklabs.studentdb.repository.UserRepository;
import com.geeklabs.studentdb.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class UserRepositoryImpl extends AbstractObjectifyCRUDRepository<User> implements UserRepository {

	@Autowired
	private Objectify objectify;

	public UserRepositoryImpl() {
		super(User.class);
	}

	@Override
	public User getUserByEmail(String userEmail) {
		 User user = objectify.load().type(User.class)
				 .filter("email", userEmail)
				 .first().now(); // with given email user might not exist, use 'now', if not exist it returns null.
		return user;
	}

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
}
