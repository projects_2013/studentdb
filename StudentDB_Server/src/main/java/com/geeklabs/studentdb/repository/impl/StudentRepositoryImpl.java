package com.geeklabs.studentdb.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.studentdb.domain.Student;
import com.geeklabs.studentdb.repository.StudentRepository;
import com.geeklabs.studentdb.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;
@Repository
public class StudentRepositoryImpl extends AbstractObjectifyCRUDRepository<Student> implements StudentRepository {

	@Autowired
	private Objectify objectify;

@Override
protected Objectify getObjectify() {
	
	return objectify;
}

@Override
public List<Student> getStudentsInfo(long studentId) {
	
	return null;
}
}