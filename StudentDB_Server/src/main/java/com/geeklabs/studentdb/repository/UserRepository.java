package com.geeklabs.studentdb.repository;

import com.geeklabs.studentdb.domain.User;
import com.geeklabs.studentdb.repository.objectify.ObjectifyCRUDRepository;

public interface UserRepository extends ObjectifyCRUDRepository<User> {
	public User getUserByEmail(String userEmail);
}
