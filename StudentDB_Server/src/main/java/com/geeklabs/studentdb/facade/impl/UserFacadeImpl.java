package com.geeklabs.studentdb.facade.impl;

import com.geeklabs.studentdb.domain.User;
import com.geeklabs.studentdb.dto.UserRegisterDto;
import com.geeklabs.studentdb.facade.UserFacade;
import com.google.api.services.oauth2.model.Tokeninfo;

public class UserFacadeImpl implements UserFacade {

	@Override
	public User convertToUserDomain(Tokeninfo tokeninfo,
			UserRegisterDto userRegisterDto) {
		// Create user
		User user = new User();
		user.setEmail(tokeninfo.getEmail());
		return user;
	}

}
