package com.geeklabs.studentdb.facade;

import com.geeklabs.studentdb.domain.User;
import com.geeklabs.studentdb.dto.UserRegisterDto;
import com.google.api.services.oauth2.model.Tokeninfo;

public interface UserFacade {

	public User convertToUserDomain(Tokeninfo tokeninfo, UserRegisterDto userRegisterDto);
}
