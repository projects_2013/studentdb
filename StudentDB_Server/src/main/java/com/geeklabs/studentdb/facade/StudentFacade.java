package com.geeklabs.studentdb.facade;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.geeklabs.studentdb.domain.Student;
import com.geeklabs.studentdb.dto.StudentDto;

public interface StudentFacade {
	public List<StudentDto> convertTrackDomainToTrackDto(List<Student> tracks);

	public Student convertTrackDtoToTrackDomain(StudentDto trackDtoList)
			throws ClientProtocolException, IOException;

}
