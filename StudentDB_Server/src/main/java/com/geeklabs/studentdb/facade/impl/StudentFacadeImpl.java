package com.geeklabs.studentdb.facade.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.dozer.DozerBeanMapper;

import com.geeklabs.studentdb.domain.Student;
import com.geeklabs.studentdb.dto.StudentDto;
import com.geeklabs.studentdb.facade.StudentFacade;

public class StudentFacadeImpl implements StudentFacade{

	private DozerBeanMapper dozerBeanMapper;
	@Override
	public List<StudentDto> convertTrackDomainToTrackDto(List<Student> students) {

		List<StudentDto> studentDtos = new ArrayList<StudentDto>();
		for (Student student : students) {
			StudentDto studentDto = new StudentDto();
			dozerBeanMapper.map(student, studentDto);
			studentDtos.add(studentDto);
		}
		return studentDtos;
	}

	@Override
	public Student convertTrackDtoToTrackDomain(StudentDto studentDto )
			throws ClientProtocolException, IOException {
		 Student student = new Student();
		 student.setEmail(studentDto.getEmail());
		 student.setId(studentDto.getId());
		 student.setName(studentDto.getName());
		return student;
	}

}
