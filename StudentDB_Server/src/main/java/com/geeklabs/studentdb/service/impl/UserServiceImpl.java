package com.geeklabs.studentdb.service.impl;

import java.io.IOException;

import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.studentdb.domain.User;
import com.geeklabs.studentdb.dto.UserRegisterDto;
import com.geeklabs.studentdb.facade.UserFacade;
import com.geeklabs.studentdb.repository.UserRepository;
import com.geeklabs.studentdb.service.UserService;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Tokeninfo;

public class UserServiceImpl implements UserService {

	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JacksonFactory FACTORY = new JacksonFactory();

	private UserRepository userRepository;
	private UserFacade userFacade;

	@Override
	@Transactional
	public UserRegisterDto registerUser(UserRegisterDto userDto)
			throws IOException {
		Tokeninfo tokeninfo = getUserTokenInfo(userDto.getAccessToken());
		if (!tokeninfo.containsKey("error")) {
			// Is user exist
			User user = userRepository.getUserByEmail(tokeninfo.getEmail());

			// Create user
			if (user == null) {
				user = userFacade.convertToUserDomain(tokeninfo, userDto);
				/*EmailService emailService = new EmailServiceImpl();
				emailService
						.send(user.getEmail(),
								"studentdb Registration",
								"Hi, \n \n Thank you for installing studentdb app."
										+ "This app captures your daily moments and it tracks Your Location by default, "
										+ "so that you can recall your past at any time, anywhere. \n \n "
										+ " Thanks \n" + " studentdb Team");
				userDto.setStatus("register");*/
			}
			userRepository.save(user);
			userDto.setUserEmail(user.getEmail());
			userDto.setUserId(user.getId());
		}
		return userDto;
	}

	private Tokeninfo getUserTokenInfo(String token) throws IOException {

		GoogleCredential credential = new GoogleCredential()
				.setAccessToken(token);
		Oauth2 oauth2 = new Oauth2.Builder(HTTP_TRANSPORT, FACTORY, credential)
				.build();
		Tokeninfo tokeninfo = oauth2.tokeninfo().setAccessToken(token)
				.execute();
		return tokeninfo;
	}

	@Override
	public UserRegisterDto updateSigninStatus(long id) {
	//	User user = userRepository.findOne(id);
		return null;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void setUserFacade(UserFacade userFacade) {
		this.userFacade = userFacade;
	}
}