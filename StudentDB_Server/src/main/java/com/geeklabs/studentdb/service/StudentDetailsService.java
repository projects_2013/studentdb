package com.geeklabs.studentdb.service;

import java.util.List;

import com.geeklabs.studentdb.dto.StudentDto;

public interface StudentDetailsService {

	List<StudentDto> getFavoriteResults(long studentId);

	

}
