package com.geeklabs.studentdb.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.studentdb.domain.Student;
import com.geeklabs.studentdb.dto.StudentDto;
import com.geeklabs.studentdb.repository.StudentRepository;
import com.geeklabs.studentdb.service.StudentDetailsService;

public class StudentDetailsServiceImple implements StudentDetailsService {
	
	private StudentRepository studentRepository;
	private DozerBeanMapper dozerBeanMapper;

	@Override
	public List<StudentDto> getFavoriteResults(long studentId) {
		Iterable<Student> students = studentRepository.findAll();
		List<StudentDto> studentDtos = new ArrayList<StudentDto>();
		Iterator<Student> iterator = students.iterator();
		while (iterator.hasNext()) {
			Student student =  iterator.next();
			StudentDto studentDto = dozerBeanMapper.map(student, StudentDto.class);
			studentDtos.add(studentDto);
			
		}
		return studentDtos;
	}
	

}
