package com.geeklabs.studentdb.service;

import java.io.IOException;

import com.geeklabs.studentdb.dto.UserRegisterDto;

public interface UserService {
	 UserRegisterDto updateSigninStatus(long id);
	 UserRegisterDto registerUser(UserRegisterDto userDto) throws IOException;
}