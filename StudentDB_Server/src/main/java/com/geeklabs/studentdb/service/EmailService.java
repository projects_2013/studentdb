package com.geeklabs.studentdb.service;

public interface EmailService {
	void send(String toEmail, String subject, String body);
}
