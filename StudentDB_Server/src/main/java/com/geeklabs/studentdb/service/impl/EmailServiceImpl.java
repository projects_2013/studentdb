package com.geeklabs.studentdb.service.impl;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.geeklabs.studentdb.service.EmailService;


public class EmailServiceImpl implements EmailService {

	private static final String FROM_EMAIL = "geekfootmark@gmail.com";

	@Override
	public void send(String toEmail, String subject, String body) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(FROM_EMAIL));
			InternetAddress to = new InternetAddress(toEmail);
			msg.addRecipient(Message.RecipientType.TO, to);
			msg.setSubject(subject);
			msg.setText(body);
			Transport.send(msg);
		} catch (AddressException e) {
			// TODO Log message  
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Log message  
			e.printStackTrace();
		}		
	}
}
