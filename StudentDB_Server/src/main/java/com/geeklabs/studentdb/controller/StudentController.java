package com.geeklabs.studentdb.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.studentdb.dto.StudentDto;
import com.geeklabs.studentdb.service.StudentDetailsService;

@Controller
@RequestMapping(value = "/student")
public class StudentController {
	
private StudentDetailsService studentDetailsService;

@RequestMapping(value = "/getStudenDetails/{studentId}", method = RequestMethod.GET)
@ResponseBody
public List<StudentDto> getFavoriteTracks(@PathVariable long studentId) {
	
	return studentDetailsService.getFavoriteResults(studentId);
}


public void setUserService(StudentDetailsService studentDetailsService) {
	this.studentDetailsService = studentDetailsService;
}
}
